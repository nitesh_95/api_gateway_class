package com.bhushan.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Author E-mail:
 * @version Creation time: 05-Jun-2021 12:19:19 pm Class Description
 */
@RestController
public class Controllers {
	@GetMapping("/welcome/{name}")
	public String wish(@PathVariable String name) {
		return "Hi " + name + " Welcome to zuul Proxy Service";
	}

}
